import React, { useEffect, useState } from 'react';
import { FlatList, View, Platform } from 'react-native';

import { LoadingIndicator, SafeAreaView } from '../../components';
import { Employee } from '../../components/Employee';
import { useListPersons } from '../../hooks/persons';
import { IEmployee } from '../../types/employee';

import styles from './styles';
import { EmployeeCard } from '../../components/EmployeeCard';

export function EmployeesScreen() {
  const [list, setList] = useState<IEmployee[]>([]);
  const { data, error, isLoading, refetch, fetchNextPage, isFetchingNextPage } =
    useListPersons();

  useEffect(() => {
    if (data) {
      setList(data.pages.flatMap(d => d.data));
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  return (
    <SafeAreaView>
      {Platform.OS === 'android' ? (
        <FlatList<IEmployee>
          refreshing={isLoading}
          numColumns={2}
          onRefresh={refetch}
          data={list}
          keyExtractor={item => item.email}
          renderItem={({ item }) => (
            <EmployeeCard
              item={{
                email: item.email,
                firstname: item.firstname,
                lastname: item.lastname,
                phone: item.phone,
                website: item.website,
              }}
            />
          )}
          onEndReachedThreshold={0.5}
          onEndReached={() => fetchNextPage()}
          ListFooterComponent={
            isLoading || isFetchingNextPage ? <LoadingIndicator /> : <></>
          }
          ItemSeparatorComponent={() => <View style={styles.separator} />}
        />
      ) : (
        <FlatList<IEmployee>
          refreshing={isLoading}
          numColumns={1}
          onRefresh={refetch}
          data={list}
          keyExtractor={item => item.email}
          renderItem={({ item }) => (
            <Employee
              item={{
                email: item.email,
                firstname: item.firstname,
                lastname: item.lastname,
                phone: item.phone,
                website: item.website,
              }}
            />
          )}
          onEndReachedThreshold={0.5}
          onEndReached={() => fetchNextPage()}
          ListFooterComponent={
            isLoading || isFetchingNextPage ? <LoadingIndicator /> : <></>
          }
          ItemSeparatorComponent={() => <View style={styles.separator} />}
        />
      )}
    </SafeAreaView>
  );
}
