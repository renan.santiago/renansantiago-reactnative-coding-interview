import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect } from 'react';
import { SafeAreaView } from '../../components';
import { ScreenParamsList } from '../../navigators/paramsList';
import { Label } from '../../components/Field/Label';

type EmployeeDetailStack = StackNavigationProp<
  ScreenParamsList,
  'EmployeeDetail'
>;
type EmployeeDetailRoute = RouteProp<ScreenParamsList, 'EmployeeDetail'>;

export function EmployeeDetailScreen() {
  const { setOptions } = useNavigation<EmployeeDetailStack>();
  const {
    params: {},
  } = useRoute<EmployeeDetailRoute>();

  useEffect(() => {
    setOptions({
      title: 'Employee Profile',
    });
  }, [setOptions]);

  return (
    <SafeAreaView>
      <Label>Profile Screen</Label>
    </SafeAreaView>
  );
}
