import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { EmployeeDetailScreen } from '../../screens/ExployeeProfile';

const Drawer = createDrawerNavigator();

export const CustomDrawer = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Profile" component={EmployeeDetailScreen} />
    </Drawer.Navigator>
  );
};

export default CustomDrawer;
