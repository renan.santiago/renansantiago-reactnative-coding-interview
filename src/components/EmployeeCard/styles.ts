import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  employeeItem: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
    width: '50%',
    borderWidth: 1,
    borderColor: '#cccccc',
    marginLeft: 5,
  },
  employeeAvatar: {
    height: 150,
    borderWidth: 1,
    borderRadius: 100,
  },
  employeeInfo: {
    justifyContent: 'space-around',
  },
});
